// Copyright (C) 2006-2007, ENPC - INRIA - EDF R&D
// Author(s): Vivien Mallet
//
// This file is part of the air quality modeling system Polyphemus.
//
// Polyphemus is developed in the INRIA - ENPC joint project-team CLIME and in
// the ENPC - EDF R&D joint laboratory CEREA.
//
// Polyphemus is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version.
//
// Polyphemus is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// For more information, visit the Polyphemus web site:
//      http://cerea.enpc.fr/polyphemus/


#ifndef POLYPHEMUS_FILE_OUTPUT_SAVER_SAVERUNITDOMAIN_AER_CXX


#include "SaverUnitDomain_aer.hxx"

#include "BaseSaverUnit.cxx"


namespace Polyphemus
{


  /////////////////////////
  // SAVERUNITDOMAIN_AER //
  /////////////////////////


  //! Main constructor.
  template<class T, class ClassModel>
  SaverUnitDomain_aer<T, ClassModel>
  ::SaverUnitDomain_aer(): BaseSaverUnit<T, ClassModel>()
  {
  }


  //! Destructor.
  template<class T, class ClassModel>
  SaverUnitDomain_aer<T, ClassModel>::~SaverUnitDomain_aer()
  {
  }


  //! Type of saver.
  /*!
    \return The string "domain_aer".
  */
  template<class T, class ClassModel>
  string SaverUnitDomain_aer<T, ClassModel>::GetType()  const
  {
    return "domain_aer";
  }


  //! First initialization.
  /*!
    \param config_stream configuration stream.
    \param Model model with the following interface:
    <ul>
    <li> GetSpeciesIndex_aer(string)
    <li> GetX_min()
    <li> GetDelta_x()
    <li> GetNx()
    <li> GetY_min()
    <li> GetDelta_y()
    <li> GetNy()
    <li> GetNz()
    <li> GetNs_aer()
    <li> GetNbin_aer()
    <li> GetSpeciesList_aer()
    <li> GetConcentration_aer()
    </ul>
  */
  template<class T, class ClassModel>
  void SaverUnitDomain_aer<T, ClassModel>::Init(ConfigStream& config_stream,
                                                ClassModel& Model)
  {
    BaseSaverUnit<T, ClassModel>::Init(config_stream, Model);

    // Checks whether the number is computed in the model.
    // Use the virtual function "HasNumberConcentration_aer" of BaseModel
    bool model_with_number = Model.HasNumberConcentration_aer();

    // Vertical levels to be saved.
    config_stream.Find("Levels");
    split(config_stream.GetLine(), levels);
    Nlevels = int(levels.size());

    // Output filenames for all species and bins.
    string filename;
    int s, b;
    if (this->species_list[0] == "all")
      {
        int Ns_aer = Model.GetNs_aer();
        vector<string> list_aer = Model.GetSpeciesList_aer();
        if (model_with_number)
          {
            list_aer.push_back("Number");
            Ns_aer++;
          }
        vector<int> bins;
        pair<string, vector<int> > tmp;
        for (b = 0; b < Model.GetNbin_aer(); b++)
          bins.push_back(b);
        tmp.second = bins;
        for (s = 0; s < Ns_aer; s++)
          {
            tmp.first = list_aer[s];
            species_list_aer.push_back(tmp);
            // Adds a buffer to compute averaged concentrations.
            Concentration_.push_back(vector<Data<T, 3> >());
            output_files.push_back(vector<string>());
            for (b = 0; b < int(bins.size()); b++)
              {
                filename = find_replace(this->output_file, "&f", tmp.first);
                filename = find_replace(filename, "&n", to_str(bins[b]));
                output_files[s].push_back(filename);
                Concentration_[s].push_back(Data<T, 3>());
              }
          }
      }
    else
      {
	string field, species;
	vector<string> vsplit, bounds;
	int first, last;
        for (unsigned int i = 0; i < this->species_list.size(); i++)
          {
            field = this->species_list[i];
            vsplit = split(field, "{}");

            if (field[0] == '{')
              throw string("Species \"") + field + string("\" is badly ")
                + "formatted: it cannot be parsed by the output saver.";
            if (vsplit.size() == 1)
              throw string("Species \"") + field + string("\" is badly ")
                + "formatted: bins are needed by the output saver.";
            if (vsplit.size() > 2)
              throw string("Species \"") + field + string("\" is badly ")
                + "formatted: it cannot be parsed by the output saver.";

	    // Searches for species index in 'species_list_aer' and
	    // 'output_files'.  If the species is not found, it is added in
	    // 'species_list_aer' and 'output_files'.
            species = split(field, "_")[0];
            s = 0;
            while (s < int(species_list_aer.size())
                   && species_list_aer[s].first != species)
              s++;
            if (s == int(species_list_aer.size()))
              {
                species_list_aer.push_back(pair<string, vector<int> >
                                           (species, vector<int>()));
                output_files.push_back(vector<string>());
                Concentration_.push_back(vector<Data<T, 3> >());
              }

            bounds = split(vsplit[1], "-");
            // First bound.
            first = convert<int>(bounds[0]);
            // Last bound.
            if (bounds.size() != 1)
              last = convert<int>(bounds[1]);
            else
              last = first;
            for (b = first; b < last + 1; b++)
              {
                // Adds the bin (associated to species #s).
                species_list_aer[s].second.push_back(b);
                // Adds a buffer to compute averaged concentrations.
                Concentration_[s].push_back(Data<T, 3>());

                // Field base name is replaced in the generic file name.
                filename = find_replace(this->output_file, "&f",
                                    species);

                // Field number is also replaced.
                filename = find_replace(filename, "&n", to_str(b));
                // Adds the corresponding output file.
                output_files[s].push_back(filename);
              }
          }

        if (model_with_number)
	  {
	    output_files.push_back(vector<string>());
	    Concentration_.push_back(vector<Data<T, 3> >());

	    vector<int> bins;
	    pair<string, vector<int> > tmp;
	    for (b = 0; b < Model.GetNbin_aer(); b++)
	      bins.push_back(b);
	    tmp.second = bins;
	    tmp.first = "Number";
	    species_list_aer.push_back(tmp);

	    int rear = output_files.size()-1;
	    for (b = 0; b < int(bins.size()); b++)
	      {
		filename = find_replace(this->output_file, "&f", "Number");
		filename = find_replace(filename, "&n", to_str(bins[b]));
		output_files[rear].push_back(filename);
		Concentration_[rear].push_back(Data<T, 3>());
	      }
	  }
      }

    // Empties output files.
    for (s = 0; s < int(species_list_aer.size()); s++)
      for (b = 0; b < int(species_list_aer[s].second.size()); b++)
	{
	  if (this->bin_output)
	    // Empties binary output file.
	    {
	      string filename = output_files[s][b];
              // Add a file extension if it has not been added.
              if (filename.substr(filename.size() - 4, 4) != ".bin")
                filename += ".bin";
	      ofstream tmp_stream(filename.c_str());
	    }
#ifdef POLYPHEMUS_WITH_NETCDF
	  if (this->netcdf_output)
	    // Empties netcdf output file with detailed metadata.
	    {
	      this->NetCDFSaver.RetrieveMetadata(*this, Model);
	      if (Nlevels < this->base_Nz)
		this->NetCDFSaver.ReduceLevels(levels);
	      string filename = output_files[s][b];
              if (filename.substr(filename.size() - 4, 4) == ".bin")
                filename = filename.substr(0, filename.size() - 4);
	      filename = filename  + ".nc";
	      this->NetCDFSaver.
		CreateFileForSpecie(filename,
				    species_list_aer[s].first,
				    species_list_aer[s].second[b]);
	    }
#endif
	}

    int k, j, i;
    // Indices in the model.
    int base_s, base_b;

    if (this->averaged)
      // Puts initial half concentrations in buffer.
      for (s = 0; s < int(species_list_aer.size()); s++)
	for (b = 0; b < int(species_list_aer[s].second.size()); b++)
	  {
	    if (species_list_aer[s].first != "Number")
	      {
		base_s = Model.GetSpeciesIndex_aer(species_list_aer[s].first);
		base_b = species_list_aer[s].second[b];
		Concentration_[s][b].Resize(Nlevels,
					    this->base_Ny, this->base_Nx);
		for (k = 0; k < Nlevels; k++)
		  for (j = 0; j < this->base_Ny; j++)
		    for (i = 0; i < this->base_Nx; i++)
		      Concentration_[s][b](k, j, i) = 0.5
			* Model.GetConcentration_aer()(base_s, base_b,
						       levels[k], j, i);
	      }
	    else if (model_with_number)
	      {
		base_b = species_list_aer[s].second[b];
		Concentration_[s][b].Resize(Nlevels,
					    this->base_Ny, this->base_Nx);
		for (k = 0; k < Nlevels; k++)
		  for (j = 0; j < this->base_Ny; j++)
		    for (i = 0; i < this->base_Nx; i++)
		      Concentration_[s][b](k, j, i) = 0.5
			* Model.GetNumberConcentration_aer()(base_b,
							     levels[k], j, i);
	      }
	  }

    if (this->initial_concentration && !this->averaged)
      Save(Model);
  }


  //! Initializes the saver at the beginning of each step.
  /*!
    \param Model model (dummy argument).
  */
  template<class T, class ClassModel>
  void SaverUnitDomain_aer<T, ClassModel>::InitStep(ClassModel& Model)
  {
    BaseSaverUnit<T, ClassModel>::InitStep(Model);
  }


  //! Saves concentrations if needed.
  /*!
    \param Model model with the following interface:
    <ul>
    <li> GetConcentration_aer()
    <li> GetSpeciesIndex_aer()
    </ul>
  */
  template<class T, class ClassModel>
  void SaverUnitDomain_aer<T, ClassModel>::Save(ClassModel& Model)
  {
    int s, b, k, j, i;
    // Indices in the model.
    int base_s, base_b;
    base_s = -999;
    string filename;
    bool model_with_number = Model.HasNumberConcentration_aer();

    if (this->averaged)
      {
        if (this->counter % this->interval_length == 0)
	  // Computed averaged concentrations will be saved.
	  {
	    double current_time =
	      Model.GetCurrentDate().GetSecondsFrom(this->date_beg);

	    for (s = 0; s < int(species_list_aer.size()); s++)
	      for (b = 0; b < int(species_list_aer[s].second.size()); b++)
		{
		  base_b = species_list_aer[s].second[b];

		  // Adds final half concentrations to buffer
		  if (species_list_aer[s].first != "Number")
		    {
		      base_s =
			Model.GetSpeciesIndex_aer(species_list_aer[s].first);
		      for (k = 0; k < Nlevels; k++)
			for (j = 0; j < this->base_Ny; j++)
			  for (i = 0; i < this->base_Nx; i++)
			    Concentration_[s][b](k, j, i) += 0.5
			      * Model.GetConcentration_aer()(base_s, base_b,
							     levels[k], j, i);
		    }
		  else if (model_with_number)
		    {
		      for (k = 0; k < Nlevels; k++)
			for (j = 0; j < this->base_Ny; j++)
			  for (i = 0; i < this->base_Nx; i++)
			    Concentration_[s][b](k, j, i) += 0.5
			      * Model.GetNumberConcentration_aer()(base_b,
								   levels[k],
								   j, i);
		    }

		  // Finishes computing averaged concentrations on interval time
		  Concentration_[s][b].GetArray() /= T(this->interval_length);

		  if (Model.GetCurrentDate() >= this->date_beg
		      && Model.GetCurrentDate() <= this->date_end)
		    {
		      if (this->bin_output)
			// Saves record into the binary output file.
			{
                          filename = output_files[s][b];
                          // Add a file extension if it has not been added.
                          if (filename.substr(filename.size() - 4, 4) != ".bin")
                            filename += ".bin";
			  FormatBinary<float>().Append(Concentration_[s][b],
						       filename);
			}
#ifdef POLYPHEMUS_WITH_NETCDF
		      if (this->netcdf_output)
			// Saves record into the netcdf output file.
			{
                          filename = output_files[s][b];
                          if (filename.substr(filename.size() - 4, 4) == ".bin")
                            filename = filename.substr(0, filename.size() - 4);
                          filename = filename  + ".nc";
			  this->NetCDFSaver.
			    AppendRecord(Concentration_[s][b],
					 species_list_aer[s].first,
					 current_time,
					 filename);
			}
#endif
		    }

		  // Puts half concentrations in buffer
		  // (for next interval computation)
		  if (species_list_aer[s].first != "Number")
		    {
		      base_s =
			Model.GetSpeciesIndex_aer(species_list_aer[s].first);
		      for (k = 0; k < Nlevels; k++)
			for (j = 0; j < this->base_Ny; j++)
			  for (i = 0; i < this->base_Nx; i++)
			    Concentration_[s][b](k, j, i) = 0.5
			      * Model.GetConcentration_aer()(base_s, base_b,
							     levels[k], j, i);
		    }
		  else if (model_with_number)
		    {
		      for (k = 0; k < Nlevels; k++)
			for (j = 0; j < this->base_Ny; j++)
			  for (i = 0; i < this->base_Nx; i++)
			    Concentration_[s][b](k, j, i) = 0.5
			      * Model.GetNumberConcentration_aer()(base_b,
								   levels[k],
								   j, i);
		    }
		}

	    this->counter = 0;
	  }

	else
	  // Simply adds concencentrations to buffer (no saving needed).
          for (s = 0; s < int(species_list_aer.size()); s++)
            for (b = 0; b < int(species_list_aer[s].second.size()); b++)
              {
                base_b = species_list_aer[s].second[b];
                if (species_list_aer[s].first != "Number")
                  {
                    base_s =
		      Model.GetSpeciesIndex_aer(species_list_aer[s].first);
                    for (k = 0; k < Nlevels; k++)
                      for (j = 0; j < this->base_Ny; j++)
                        for (i = 0; i < this->base_Nx; i++)
                          Concentration_[s][b](k, j, i) +=
                            Model.GetConcentration_aer()(base_s, base_b,
                                                         levels[k], j, i);
                  }
                else if (model_with_number)
                  {
                    for (k = 0; k < Nlevels; k++)
                      for (j = 0; j < this->base_Ny; j++)
                        for (i = 0; i < this->base_Nx; i++)
                          Concentration_[s][b](k, j, i) +=
                            Model.GetNumberConcentration_aer()(base_b,
                                                               levels[k], j, i);
                  }
              }
      }

    else if (this->counter % this->interval_length == 0
             && Model.GetCurrentDate() >= this->date_beg
             && Model.GetCurrentDate() <= this->date_end)
      // Instantaneous concentrations.
      {
	Data<T, 3> Concentration_tmp;
	Data<T, 3> NumberConcentration_tmp;
	double current_time =
	  Model.GetCurrentDate().GetSecondsFrom(this->date_beg);

	for (s = 0; s < int(species_list_aer.size()); s++)
	  if (species_list_aer[s].first != "Number")
	    for (b = 0; b < int(species_list_aer[s].second.size()); b++)
	      {
		base_s = Model.GetSpeciesIndex_aer(species_list_aer[s].first);
		base_b = species_list_aer[s].second[b];

		for (k = 0; k < Nlevels; k++)
		  for (j = 0; j < this->base_Ny; j++)
		    for (i = 0; i < this->base_Ny; i++)
		      {
			Concentration_tmp(k, j, i) =
			  Model.GetConcentration_aer()(base_s, base_b,
						       levels[k], j, i);
			if (this->bin_output)
			  // Saves record into the binary output file.
			  {
                            filename = output_files[s][b];
                            // Add a file extension if it has not been added.
                            if (filename.substr(filename.size() - 4, 4) != ".bin")
                              filename += ".bin";
			    FormatBinary<float>().Append(Concentration_tmp,
							 filename);
			  }
#ifdef POLYPHEMUS_WITH_NETCDF
			if (this->netcdf_output)
			  // Saves record into the netcdf output file.
			  {
                            filename = output_files[s][b];
                            if (filename.substr(filename.size() - 4, 4) == ".bin")
                              filename = filename.substr(0, filename.size() - 4);
                            filename = filename  + ".nc";
			    this->NetCDFSaver.
			      AppendRecord(Concentration_tmp,
					   species_list_aer[s].first,
					   current_time,
					   filename);
			}
#endif
		      }
	      }
	  else if (model_with_number)
	    for (b = 0; b < int(species_list_aer[s].second.size()); b++)
	      {
		base_b = species_list_aer[s].second[b];
		for (k = 0; k < Nlevels; k++)
		  for (j = 0; j < this->base_Ny; j++)
		    for (i = 0; i < this->base_Ny; i++)
		      {
			NumberConcentration_tmp(k, j, i) =
			  Model.GetNumberConcentration_aer()(base_b,
							     levels[k], j, i);
			if (this->bin_output)
			  // Saves record into the binary output file.
			  {
                            filename = output_files[s][b];
                            // Add a file extension if it has not been added.
                            if (filename.substr(filename.size() - 4, 4) != ".bin")
                              filename += ".bin";
			    FormatBinary<float>().
			      Append(NumberConcentration_tmp,filename);
			  }
#ifdef POLYPHEMUS_WITH_NETCDF
			if (this->netcdf_output)
			  // Saves record into the netcdf output file.
			  {
                            filename = output_files[s][b];
                            if (filename.substr(filename.size() - 4, 4) == ".bin")
                              filename = filename.substr(0, filename.size() - 4);
			    filename = output_files[s][b] + ".nc";
			    this->NetCDFSaver.
			      AppendRecord(NumberConcentration_tmp,
					   species_list_aer[s].first,
					   current_time,
					   filename);
			  }
#endif
		      }
	      }
      }
  }

} // namespace Polyphemus.


#define POLYPHEMUS_FILE_OUTPUT_SAVER_SAVERUNITDOMAIN_AER_CXX
#endif
