// Copyright (C) 2005-2007, ENPC - INRIA - EDF R&D
// Author(s): Camille Daniel
//
// This file is part of the air quality modeling system Polyphemus.
//
// Polyphemus is developed in the INRIA - ENPC joint project-team CLIME and in
// the ENPC - EDF R&D joint laboratory CEREA.
//
// Polyphemus is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version.
//
// Polyphemus is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// For more information, visit the Polyphemus web site:
//      http://cerea.enpc.fr/polyphemus/

#ifndef POLYPHEMUS_FILE_OUTPUT_SAVER_NETCDFOUTPUT_HXX

#include <string>
#include <blitz/array.h>
#include "SeldonDataHeader.hxx"

namespace Polyphemus
{

  //////////////////
  // NETCDFOUTPUT //
  //////////////////

  /*! \brief This class is used to create and manipulate netCDF output files.
   */

  template<class T>
  class NetCDFOutput
  {

  protected:
    //! Cartesian domain?
    bool cartesian;

    //! Domain coordinates.
    //! Array of Z coordinates (levels).
    blitz::Array<T, 1> ZArray;
    //! Array of Z coordinate bounds.
    blitz::Array<T, 2> ZArrayBnds;
    //! Array of Y coordinates.
    blitz::Array<T, 1> YArray;
    //! Array of Y coordinate bounds.
    blitz::Array<T, 2> YArrayBnds;
    //! Array of X coordinates.
    blitz::Array<T, 1> XArray;
    //! Array of X coordinate bounds.
    blitz::Array<T, 2> XArrayBnds;

    //! Time units.
    std::string time_units;
    //! Time bounds?
    bool with_time_bounds;
    //! Time method (mean or point).
    std::string time_method;
    //! Time interval between 2 savings.
    double time_interval;

    //! Concentration units.
    std::string concentration_units;
    //! Aerosol?
    bool aerosol;

    //! Polyphemus version.
    std::string polyphemus_version;

  public:

    NetCDFOutput();
    ~NetCDFOutput();

    template<class ClassSaverUnit, class ClassModel>
    void RetrieveMetadata(ClassSaverUnit& SaverUnit, ClassModel& Model);

    void ReduceLevels(vector<int> levels);
    void ReduceHorizontalDomain(int i_min, int i_max, int j_min, int _max);

    void CreateFileForSpecie(std::string filename, std::string specie,
			     int bin = -1);

    template<class TD, int N, class TG>
    void AppendRecord(SeldonData::Data<TD, N, TG>& RecordData,
		      std::string specie, double time, std::string filename);

  };


} // namespace Polyphemus.

#define POLYPHEMUS_FILE_OUTPUT_SAVER_NETCDFOUTPUT_HXX
#endif
