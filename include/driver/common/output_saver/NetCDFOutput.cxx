// Copyright (C) 2005-2007, ENPC - INRIA - EDF R&D
// Author(s): Camille Daniel
//
// This file is part of the air quality modeling system Polyphemus.
//
// Polyphemus is developed in the INRIA - ENPC joint project-team CLIME and in
// the ENPC - EDF R&D joint laboratory CEREA.
//
// Polyphemus is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version.
//
// Polyphemus is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// For more information, visit the Polyphemus web site:
//      http://cerea.enpc.fr/polyphemus/

#ifndef POLYPHEMUS_FILE_OUTPUT_SAVER_NETCDFOUTPUT_CXX

#ifndef SELDONDATA_WITH_NETCDF
#define SELDONDATA_WITH_NETCDF
#endif
#include "SeldonData.hxx"

#include "NetCDFOutput.hxx"

namespace Polyphemus
{

  using namespace SeldonData;

  //////////////////
  // NETCDFOUTPUT //
  //////////////////

  //! Main constructor.
  template<class T>
  NetCDFOutput<T>::NetCDFOutput()
  {
  }

  //! Destructor.
  template<class T>
  NetCDFOutput<T>::~NetCDFOutput()
  {
  }

  //! Retrieves metadata.
  /*! Initializes all member variables. They will be used to write metadata
    when creating new file.
    In particular, initializes domain coordinates with model domain.
    \param SaverUnit saver unit with the following interface:
    <ul>
    <li> GetType()
    <li> GetLevels()
    <li> GetDate_beg()
    <li> GetAveraged()
    <li> GetInterval_length()
    </ul>
    \param Model model with the following interface:
    <ul>
    <li> GetConfigurationFile()
    <li> GetLayerInterface()
    <li> GetGridZArray1D()
    <li> GetGridYArray1D()
    <li> GetGridXArray1D()
    <li> GetDelta_t()
    </ul>
  */
  template<class T>
  template<class ClassSaverUnit, class ClassModel>
  void NetCDFOutput<T>::RetrieveMetadata(ClassSaverUnit& SaverUnit,
					 ClassModel& Model)
  {

    // Cartesian domain?
    ConfigStream config(Model.GetConfigurationFile());
    if (config.Find("Cartesian"))
      config.GetElement(cartesian);
    else
      // If there is no field named "Cartesian" in the main config file,
      // domain is assumed to be cartesian.
      cartesian = true;

    // Z, Y, X coordinates of model domain as references.
    Array<T, 1> Base_LayerInterface(Model.GetLayerInterface());
    Array<T, 1> Base_ZArray(Model.GetGridZArray1D());
    Array<T, 1> Base_YArray(Model.GetGridYArray1D());
    Array<T, 1> Base_XArray(Model.GetGridXArray1D());

    // Z, Y, X coordinates and bounds of saver domain based upon model domain
    ZArray.resize(Base_ZArray.shape());
    ZArray = Base_ZArray;

    ZArrayBnds.resize(ZArray.size(), 2);
    int end = Base_LayerInterface.size() - 1;
    ZArrayBnds(Range::all(), 0) = Base_LayerInterface(Range(0, end -1));
    ZArrayBnds(Range::all(), 1) = Base_LayerInterface(Range(1, end));

    YArray.resize(Base_YArray.shape());
    YArray = Base_YArray;

    YArrayBnds.resize(YArray.size(), 2);
    T delta_y = YArray(1) - YArray(0);
    YArrayBnds(Range::all(), 0) = YArray - delta_y/2;
    YArrayBnds(Range::all(), 1) = YArray + delta_y/2;

    XArray.resize(Base_XArray.shape());
    XArray = Base_XArray;

    XArrayBnds.resize(XArray.size(), 2);
    T delta_x = XArray(1) - XArray(0);
    XArrayBnds(Range::all(), 0) = XArray - delta_x/2;
    XArrayBnds(Range::all(), 1) = XArray + delta_x/2;

    // Time units.
    string time_ref = SaverUnit.GetDate_beg().
      Talos::Date::GetDate("%y-%m-%d %h:%i:%s");
    time_units = "seconds since " + time_ref;

    // Time bounds.
    with_time_bounds = SaverUnit.GetInterval_length() > 1;

    // Time method.
    if (SaverUnit.GetAveraged())
      time_method = "time: mean";
    else
      time_method = "time: point";
    time_method += " (interval: " + Talos::to_str(Model.GetDelta_t()) + " sec)";

    // Time interval between 2 savings.
    time_interval = Model.GetDelta_t()*SaverUnit.GetInterval_length();

    // Concentration units.
    // TODO: récupérer l'info dans un fichier config.

    // Aerosol?
    aerosol = (SaverUnit.GetType().find("aer") != std::string::npos);

    // Polyphemus version.
    // TODO: faire en sorte que l'info soit récupérée dans le fichier version.
    polyphemus_version = "Polyphemus v1.10";

  }


 //! Reduces levels of domain.
  template<class T>
  void NetCDFOutput<T>::ReduceLevels(vector<int> levels)
  {

    Array<T, 1> Previous_ZArray = ZArray.copy();
    Array<T, 2> Previous_ZArrayBnds = ZArrayBnds.copy();

    ZArray.resize(levels.size());
    ZArrayBnds.resize(ZArray.size(), 2);

    for (int k = 0; k < int(levels.size()); k++)
      {
	ZArray(k) = Previous_ZArray(levels[k]);
	ZArrayBnds(k, Range::all()) = Previous_ZArrayBnds(levels[k],
							  Range::all());
      }

  }


  //! Reduces horizontal domain (for subdomain saving).
  template<class T>
  void NetCDFOutput<T>::ReduceHorizontalDomain(int i_min, int i_max,
					       int j_min, int j_max)
  {

    Array<T, 1> Previous_YArray = YArray.copy();
    Array<T, 2> Previous_YArrayBnds = YArrayBnds.copy();
    Array<T, 1> Previous_XArray = XArray.copy();
    Array<T, 2> Previous_XArrayBnds = XArrayBnds.copy();

    YArray.resize(i_max - i_min + 1);
    YArray = Previous_YArray(Range(i_min, i_max));
    YArrayBnds.resize(i_max - i_min + 1, 2);
    YArrayBnds = Previous_YArrayBnds(Range(i_min, i_max), Range::all());

    XArray.resize(j_max - j_min + 1);
    XArray = Previous_XArray(Range(j_min, j_max));
    XArrayBnds.resize(j_max - j_min + 1, 2);
    XArrayBnds = Previous_XArrayBnds(Range(j_min, j_max), Range::all());

  }


  //! Creates a netCDF file for a specie with detailed metadata.
  // bin is an optional parameter, only used with aerosol.
  template<class T>
  void NetCDFOutput<T>::CreateFileForSpecie(string filename, string specie,
					    int bin)
  {

    FormatNetCDF<float> FormatNetCDF;

    // Defines the global attributes.
    vector<pair<string, string> > attributes;
    pair<string, string> att1 ("version", polyphemus_version);
    attributes.push_back(att1);
    FormatNetCDF.SetGlobalAttributes(attributes);

    // Names Y, X coordinates.
    string y_name, x_name;
    if (cartesian)
      y_name = "Y", x_name = "X";
    else
      y_name = "latitude", x_name = "longitude";

    // Defines the dimensions.
    vector<pair<string, int> > dimensions;
    pair<string, int> zDim ("level", ZArray.size());
    dimensions.push_back(zDim);
    pair<string, int> yDim (y_name, YArray.size());
    dimensions.push_back(yDim);
    pair<string, int> xDim (x_name, XArray.size());
    dimensions.push_back(xDim);
    pair<string, int> nvDim ("nv", 2);
    dimensions.push_back(nvDim);
    pair<string, int> timeDim ("time", -1);
    dimensions.push_back(timeDim);
    FormatNetCDF.SetDimensions(dimensions);

    // Defines the coordinate variables.
    vector<pair<string, vector<string> > > variables;
    pair<string, vector<string> > zVar;
    zVar.first = "level";
    zVar.second.push_back("level");
    variables.push_back(zVar);
    pair<string, vector<string> > yVar;
    yVar.first = y_name;
    yVar.second.push_back(y_name);
    variables.push_back(yVar);
    pair<string, vector<string> > xVar;
    xVar.first = x_name;
    xVar.second.push_back(x_name);
    variables.push_back(xVar);
    pair<string, vector<string> > timeVar;
    timeVar.first = "time";
    timeVar.second.push_back("time");
    variables.push_back(timeVar);

    // Defines the boundary variables.
    pair<string, vector<string> > zBndsVar;
    zBndsVar.first = "level_bounds";
    zBndsVar.second.push_back("level");
    zBndsVar.second.push_back("nv");
    variables.push_back(zBndsVar);
    pair<string, vector<string> > yBndsVar;
    yBndsVar.first = y_name + "_bounds";
    yBndsVar.second.push_back(y_name);
    yBndsVar.second.push_back("nv");
    variables.push_back(yBndsVar);
    pair<string, vector<string> > xBndsVar;
    xBndsVar.first = x_name + "_bounds";
    xBndsVar.second.push_back(x_name);
    xBndsVar.second.push_back("nv");
    variables.push_back(xBndsVar);
    if (with_time_bounds)
      {
	pair<string, vector<string> > timeBndsVar;
	timeBndsVar.first = "time_bounds";
	timeBndsVar.second.push_back("time");
	timeBndsVar.second.push_back("nv");
	variables.push_back(timeBndsVar);
      }

    // Defines the record variable (specie concentration)
    pair<string, vector<string> > recVar;
    recVar.first = specie;
    recVar.second.push_back("time");
    recVar.second.push_back("level");
    recVar.second.push_back(y_name);
    recVar.second.push_back(x_name);
    variables.push_back(recVar);

    FormatNetCDF.SetVariables(variables);

    // Defines variable attributes.
    vector<pair<string, string> > zVarAttributes;
    pair<string, string> varAtt1("long_name", "level");
    zVarAttributes.push_back(varAtt1);
    pair<string, string> varAtt2("units", "meters");
    zVarAttributes.push_back(varAtt2);
    pair<string, string> varAtt3("positive", "up");
    zVarAttributes.push_back(varAtt3);
    pair<string, string> varAtt4("axis", "Z");
    zVarAttributes.push_back(varAtt4);
    pair<string, string> varAtt5("bounds", "level_bounds");
    zVarAttributes.push_back(varAtt5);
    FormatNetCDF.SetAttributesToVariable(zVarAttributes, "level");

    vector<pair<string, string> > yVarAttributes;
    varAtt1.second = y_name;
    yVarAttributes.push_back(varAtt1);
    varAtt2.second = "degrees_north";
    yVarAttributes.push_back(varAtt2);
    varAtt4.second = "Y";
    yVarAttributes.push_back(varAtt4);
    varAtt5.second = y_name + "_bounds";
    yVarAttributes.push_back(varAtt5);
    FormatNetCDF.SetAttributesToVariable(yVarAttributes, y_name);

    vector<pair<string, string> > xVarAttributes;
    varAtt1.second = x_name;
    xVarAttributes.push_back(varAtt1);
    varAtt2.second = "degrees_east";
    xVarAttributes.push_back(varAtt2);
    varAtt4.second = "X";
    xVarAttributes.push_back(varAtt4);
    varAtt5.second = x_name + "_bounds";
    xVarAttributes.push_back(varAtt5);
    FormatNetCDF.SetAttributesToVariable(xVarAttributes, x_name);

    vector<pair<string, string> > timeVarAttributes;
    varAtt1.second = "time";
    timeVarAttributes.push_back(varAtt1);
    varAtt2.second = time_units;
    timeVarAttributes.push_back(varAtt2);
    varAtt3.first = "calendar", varAtt3.second = "standard";
    timeVarAttributes.push_back(varAtt3);
    varAtt4.second = "T";
    timeVarAttributes.push_back(varAtt4);
    if (with_time_bounds)
      {
	varAtt5.second = "time_bounds";
	timeVarAttributes.push_back(varAtt5);
      }
    FormatNetCDF.SetAttributesToVariable(timeVarAttributes, "time");

    vector<pair<string, string> > recVarAttributes;
    varAtt1.second = "mass concentration of " + specie;
    if (aerosol)
      varAtt1.second += " aerosol particles";
    varAtt1.second += " in air";
    recVarAttributes.push_back(varAtt1);
    varAtt2.second = concentration_units;
    recVarAttributes.push_back(varAtt2);
    if (aerosol)
      {
	varAtt3.first = "bin", varAtt3.second = Talos::to_str(bin);
	recVarAttributes.push_back(varAtt3);
      }
    varAtt4.first = "cell_methods", varAtt4.second = time_method;
    recVarAttributes.push_back(varAtt4);
    FormatNetCDF.SetAttributesToVariable(recVarAttributes, specie);

    // Creates empty structured netCDF file
    FormatNetCDF.CreateStructuredFile(filename);

    // Writes coordinate and boundary variables
    FormatNetCDF.Write(ZArray, "level", filename);
    FormatNetCDF.Write(YArray, y_name, filename);
    FormatNetCDF.Write(XArray, x_name, filename);
    FormatNetCDF.Write(ZArrayBnds, "level_bounds", filename);
    FormatNetCDF.Write(YArrayBnds, y_name + "_bounds", filename);
    FormatNetCDF.Write(XArrayBnds, x_name + "_bounds", filename);

  }


//! Appends data to the specie record variable in its netCDF file.
  template<class T>
  template<class TD, int N, class TG>
  void NetCDFOutput<T>::AppendRecord(Data<TD, N, TG>& RecordData,
				     string specie, double time,
				     string filename)
  {

    if (with_time_bounds)
      {
	// Creates the array time_bounds.
	Data<T, 2> TimeBounds(1, 2);
	if (time == 0)
	  TimeBounds(0, 0) = 0;
	else
	  TimeBounds(0, 0) = time - time_interval;
	TimeBounds(0, 1) = time;

	// Appends data
	FormatNetCDF<float>().AppendRecordWithBounds(RecordData, specie, time,
						     TimeBounds, filename);
      }
    else
      FormatNetCDF<float>().AppendRecord(RecordData, specie, time, filename);

  }

} // namespace Polyphemus.

#define POLYPHEMUS_FILE_OUTPUT_SAVER_NETCDFOUTPUT_CXX
#endif
