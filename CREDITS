The list of developers is provided in *alphabetic* order.

+ Meryem Ahmed de Biasi
  Usability and software distribution
  Decay module and output saver for nesting
  Parallel computing

+ Mohamed Aissaoui
  Random perturbations

+ Régis Briant
  Gaussian plume model (discretized line sources)
  Improvement of the hybrid model "plume in grid"

+ Florian Couvidat
  Organic aerosol formation in SIREAM-AEC (H2O) aerosol module.
  MEGAN biogenic emission preprocessing
  Secondary organic aerosol processor (SOAP)

+ Edouard Debry
  Original author of SIREAM and SIREAM-AEC (aerosol modules)
  Aerosol-specific preprocessing

+ Stéphanie Deschamps
  Aerosol number concentration processing 
 
+ Sylvain Doré
  Release manager
  Bug fixes and refactoring

+ Kathleen Fahey
  Aqueous-phase model

+ Hadjira Foudhil
  Local scale

+ Damien Garaud
  Ensemble simulations

+ Karine Kata-Sartelet
  Aerosol-specific preprocessing
  Contributions to SIREAM

+ Youngseob Kim
  Release manager
  Chemical mecanisms RACM2 and CB05
  Biogenic precursors and explicitly NOx regime in SIREAM-SORGAM aerosol module
  Plume-in-grid with Gaussian puff aerosol model
  MUNICH street network model
  Street-in-grid with MUNICH street network model

+ Irène Korsakissok
  Gaussian plume model and its preprocessing
  Gaussian puff model
  Plume in grid model
  Miscellaneous model developments

+ Vivien Mallet
  Polyphemus original author
  Libraries Talos, SeldonData, AtmoData and AtmoPy
  Preprocessing (except for aerosols)
  Castor and Polair3D
  Postprocessing (except for aerosols)
  Ensemble methods
  Data assimilation
  Uncertainty estimation
  Local scale

+ Boris Mauricette
  Ensemble methods

+ Hervé Njomgang
  MM5 utilities and ground data

+ Vincent Picavet
  AtmoPy
  Contributions to SeldonData and to preprocessing

+ Denis Quélo
  Miscellaneous developments and improvements
  Numerical schemes in Polair3D
  Preprocessing (emissions, ground data, Castor)

+ Elsa Real
  FastJX photolysis rates scheme in preprocessing and siream-sorgam
  "Aircraft" observations and cross-section plots in Atmopy.

+ Yelva Roustan
  Aerosol deposition and scavenging
  Miscellaneous model developments

+ Chi-Sian Soh
  Dry deposition for gas

+ Bruno Sportisse
  Aqueous-phase model
  Contributions to SIREAM
  Chemical mechanisms

+ Gilles Stoltz
  Ensemble methods

+ Marilyne Tombette
  Aerosol optical parameters
  Aerosol-specific preprocessing and postprocessing
  Aqueous-phase model
  Contributions to SIREAM

+ Pierre Tran
  Release manager
  Parallel computing
  Lagrangian model

+ Victor Winiarek
  Preprocessing (WRF)

+ Lin Wu
  Data assimilation

+ Shupeng Zhu
  External mixing aerosol module SCRAM

People who helped with minor contributions, old contributions (now removed),
with advice, or who provided code from which parts of Polyphemus are derived
(alphabetic order): Marc Bocquet, Christelle Bordas, Jaouad Boutahar, Chimere
team for Castor (developers: Laurent Menut, Jean-Louis Monge, Robert Vautard),
Cécile Honoré, Luc Musson-Genon, Pierre Plion, Christian Seigneur, Germán
Torres, Nikki Vercauteren, Denis Wendum.
