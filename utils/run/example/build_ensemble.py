import sys
sys.path.insert(0, '../')
from ensemble_generation import EnsembleParameter
sys.path.pop(0)

######################################
# An example to build a new ensemble #
######################################

# Reads parameter.cfg
p = EnsembleParameter("parameter.cfg")

# You have p.name and p.value for the names of your parameters and the
# different values for each parameter.

# Prints the model number.
print "Nmodel: ", p.Nmodel

# Generates a new ensemble.
p.GenerateIdEnsemble()

# Now, you have Nmodel identities in p.id_ensemble (a list of list of
# integers).

# Checks the parameter frequency.
fq_dict = p.GetParameterFrequency()
print "\nParameter Frequency : "
for k in fq_dict.keys():
    tmp = ""
    for i in range(len(fq_dict[k])):
        tmp += "\t %.1f " % round(fq_dict[k][i] * 100.)
    print(k + ': ' + tmp)

# You can write your ensemble in a file.
p.WriteIdEnsembleInFile("id_ensemble.dat")
print "\nYou have a new ensemble in the file \"id_ensemble.dat\"."



