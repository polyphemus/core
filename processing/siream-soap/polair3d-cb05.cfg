[display]

Show_iterations: yes
Show_date: yes
Show_configuration: yes

[domain]

Date_min = 2012-05-15+00-00
Delta_t = 600.   Nt = 144
x_min   = -15.0  Delta_x = 0.5    Nx = 100
y_min   =  35.0  Delta_y = 0.5    Ny = 70
Nz = 9
Vertical_levels: levels.dat

# If the domain is not Cartesian, coordinates
# are assumed to be latitude/longitude.
Cartesian: no

# Species list and associated data.
Species: species-cb05.dat
Bin_bounds:
0.01 0.0398 0.1585 0.6310 2.5119 10.


[options]

# Main processes.
With_advection: yes
With_diffusion: yes
With_chemistry: yes
With_photolysis: yes
With_tabulated_photolysis: yes

# How photolysis rates are computed:
# 'no' no computation, photolysis is tabulated.
# 'preproc' photolysis is computed during preprocessing.
# 'on-line' photolysis is computed on the fly, during the simulation.
Computed_photolysis: no

# In case photolysis rates are tabulated, how was it generated:
# 1. by SPACK.
# 2. by FastJ.
Photolysis_tabulation_option: 2


With_forced_concentration: no
Source_splitting: no
With_number_concentration: yes
 
# Air density may be used to diagnose the vertical wind
# and included in the diffusion term.
With_air_density: no

# Use initial conditions? All concentrations are set to zero otherwise.
With_initial_condition: yes
With_initial_condition_aerosol: yes
With_initial_condition_number_aerosol: no 

# Lateral and top boundary conditions.
With_boundary_condition: yes
With_boundary_condition_aerosol: yes
With_boundary_condition_number_aerosol: no 

# Other processes.
With_deposition: yes
With_point_emission: no
With_surface_emission: yes
With_additional_surface_emission: yes
With_volume_emission: yes

# Should be set to true only if chemistry computes pH.
With_pH: yes

# Below-cloud scavenging model: none, constant, belot, microphysical or microphysical-ph.
Scavenging_below_cloud_model: microphysical-ph
# In-cloud scavenging model: none, belot or pudykiewicz.
Scavenging_in_cloud_model: none

# Should deposition fluxes be collected?
# Put "yes" if you want an output saver to save them.
Collect_dry_flux: no
Collect_wet_flux: no
Collect_dry_flux_aerosol: no
Collect_wet_flux_aerosol: no

# For aerosols.
With_deposition_aerosol: yes
Compute_deposition_aerosol: yes
With_scavenging_aerosol: yes
With_point_emission_aerosol: no
With_surface_emission_aerosol: yes
With_surface_number_emission_aerosol: no
With_volume_emission_aerosol: yes
With_volume_number_emission_aerosol: no

# Aerosol 0D options:
With_coagulation: yes
With_condensation: yes
With_nucleation: no

# Liquid water content threshold above which cloud is present (in g/m3).
Lwc_cloud_threshold: 0.05

# Aqueous module: 'No', 'VSRM' or 'simple'.
aqueous_module: simple

# Does scavenging occures in clouds?
With_in_cloud_scavenging: yes

# Are heterogeneous reactions taking place at aerosol surface?
With_heterogeneous_reactions: yes

# Does condensation take into account the Kelvin effect?
With_kelvin_effect: yes

# Numerical solver for dynamic bin condensation (etr, ros2 or ebi).
Dynamic_condensation_solver: ros2

# Cutting diameter between equilibrium and dynamic bins (in micrometer).
Fixed_cutting_diameter: 10.0

# Sulfate condensation computation method (equilibrium, dynamic).
Sulfate_computation: dynamic

# Redistribution method of lagrangian bins (number-conserving, interpolation). 
# (euler-mass, euler-number, hemen, euler-coupled, moving-diameter). 
Redistribution_method: moving-diameter
With_normalisation: no
Conserving_mass_tolerance: 0. 

# If nucleation, which nucleation model (binary, ternary).
Nucleation_model: ternary
# If unary, kuang parameterization.
Nucleation_prefactor:1.26e-14
Nucleation_exponent:2.01

# Is the density assumed to be constant in all cells and all bins?
With_fixed_density: yes

# Fixed aerosol density (kg / m^3).
Fixed_aerosol_density: 1400.

# Computation method for aerosol wet diameter: 'Gerber' or 'Isorropia'.
Wet_diameter_estimation: Gerber

# With oligomerization?
With_oligomerization: yes

# Thermodynamic model for organic activity coefficients computation:
# 'aiomfac', 'unifac' or 'ideal'.
Organic_thermodynamic_model: unifac

# With adaptive time stepping for gaseous chemistry?
With_adaptive_time_step_for_gas_chemistry: yes
Adaptive_time_step_tolerance: 0.001
# Minimum time step that can be used.
Min_adaptive_time_step: 10.0
# Maximum time step that can be used.
Max_adaptive_time_step: 600.0

# SOA computation method: 'equilibrium' or 'dynamic'.
SOA_computation: equilibrium
# Diffusion coefficient in the organic phase if it is constant : m^2 / s.
# It is necessary when the dynamic method is used.
Diffusion_coefficient_organic: 1.0e-12
# Number of aerosol layers (1 to 5) when the dynamic method is used.
Number_of_aerosol_layers: 1


[computing]

# Number of threads if openMP parallelization is used.
Number_of_threads_openmp: 1


[data]

Data_description: polair3d-data-cb05.cfg

Horizontal_diffusion = 10000.
# In case diffusion coefficient is isotropic, diffusion coefficient is set
# to the vertical diffusion coefficient in all directions?
Isotropic_diffusion: no

Land_data: glc2000.dat


[output]

Configuration_file: polair3d-saver-cb05.cfg
