# Copyright (C) 2008, ENPC - INRIA - EDF R&D
#
# This file is part of the air quality modeling system Polyphemus.
#
# Polyphemus is developed in the INRIA - ENPC joint project-team CLIME and in
# the ENPC - EDF R&D joint laboratory CEREA.
#
# Polyphemus is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# Polyphemus is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# For more information, visit the Polyphemus web site:
#      http://cerea.enpc.fr/polyphemus/

# Put the path to Polyphemus.
# Also editable from command line with option "polyphemus".
polyphemus_path = "../../"

utils = SConscript(polyphemus_path + '/scons/utils')(globals())


# Include paths: put the absolute paths, the relative paths or the paths into
# the Polyphemus directory.
include_path_template = """
include/common
include/Talos
include/SeldonData
include/AtmoData
include/modules/common
include/modules/transport
include/modules/transport/SplitAdvectionDST3
include/modules/transport/DiffusionROS2
include/models
include/driver/common
include/driver/common/output_saver
"""

# [Optional] Compiled library paths: put absolute paths, relative paths or
# paths that are in the Polyphemus directory. Put the list of paths to
# compiled libraries that may not be installed in your system root.
library_path = None

exclude_dependency = ["typeaoutput\.c", "typeboutput\.c", "read_shm\.c",
                      "set_shm\.c", "write_shm\.c", "read_shm_aer\.c",
                      "write_shm_aer\.c" ]

include_path = include_path_template.format()
utils.create_programs()

# Avoids displaying the libraries checking next time since libraries
# does not change in this case.
utils.run.quiet(True)
