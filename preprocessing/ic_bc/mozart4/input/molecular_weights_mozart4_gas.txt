O3        48.0
N2O       44.0
NO        30.0
NO2       46.0
NO3       62.0
HNO3      63.0
HO2NO2    79.0
N2O5      108.0
OH        17.0
HO2       33.0
H2O2      34.0
CH4       16.0
CO        28.0
CH3O2     47.0
CH3OOH    48.0
CH2O      30.0
CH3OH     32.0
C2H5OH    46.0
C2H4      28.0
CH3COOH   60.0
GLYALD    60.0
C2H6      30.0
CH3CHO    44.0
CH3COOOH  76.0
C3H6      42.0
C3H8      44.0
CH3COCH3  58.0
BIGENE    56.0
MEK       72.0
BIGALK    72.0
ISOP      68.0
MVK       70.0
MACR      70.0
HYDRALD   100.0
HYAC      74.0
CH3COCHO  72.0
C10H16    136.0
TOLUENE   92.0
CRESOL    108.0
BIGALD    98.0
PAN       121.0
ONIT      119.0
MPAN      147.0
ISOPNO3   162.0
ONITR     147.0
SO2       64.0
DMS       62.0
SO4       96.0
NH3       17.0
